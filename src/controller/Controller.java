package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller <T extends Number>{

	private static NumbersBagOperations<Number> model= new NumbersBagOperations<Number>();
	
	
	public static NumbersBag createBag(ArrayList<Number> values){
         return new NumbersBag(values);		
	}
	
	
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static Number add(NumbersBag bag,Number value){
		return model.add(bag,value);
	}
	
	public static NumbersBag remove(NumbersBag bag,Number value){
		return model.remove(bag, value);
	}
	
	public static int size(NumbersBag bag){
		return model.size(bag);
	}
	
}
