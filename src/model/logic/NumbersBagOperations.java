package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations <T extends Number>{

	
	
	public double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(NumbersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public T add(NumbersBag bag,T datum){
		bag.addDatum(datum);
		return datum;
	}
	
	public NumbersBag remove(NumbersBag bag,T datum){
		NumbersBag h = new NumbersBag();
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				T value = iter.next();
					if(datum!=value){
						add(h,value);
				}
			}
			
		}
		return h;
	}
	
	public int size(NumbersBag bag){
		int cont = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				cont++;
				iter.next();
			}
		}
		return cont;
	}
}
